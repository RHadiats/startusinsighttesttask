import gspread
import csv
import pandas as pd
import argparse

from gspread_dataframe import set_with_dataframe
from oauth2client.service_account import ServiceAccountCredentials


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("--urls", required=True, help="path to file with urls data")
    ap.add_argument("--data", required=True, help="path to file with parsed data")
    args = vars(ap.parse_args())

    SCOPE = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    CREDENTIALS = 'PASTE_YOUR_CREDENTIALS_FIRSTLY_IF_YOU_WANT_TO_TEST_THIS_SCRIPT.json'
    SHEET_NAME = "Website crawler v.3.0 – ROMAN HADIATSKYI"

    creds = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS, SCOPE)
    client = gspread.authorize(creds)

    sheet = client.open(SHEET_NAME)
    worksheets = {
        args.get('urls'): sheet.worksheet('Result urls'),
        args.get('data'): sheet.worksheet('Result data')
    }

    for filename, worksheet in worksheets.items():
        with open(filename) as f:
            rows = csv.reader(f, delimiter=',')
            header = next(rows, None)

            df = pd.DataFrame(rows, columns=header)
            set_with_dataframe(worksheet, df)
            print(filename, 'sheet is exported')
