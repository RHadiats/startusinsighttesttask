import scrapy
import json

from ..items import UrlsCollectorItem


class UrlsCollectorSpider(scrapy.Spider):
    name = "urls_collector"

    def start_requests(self):
        self.settings.start_count = 0
        self.settings.length_count = 1000

        urls = [
            'https://e27.co/api/startups/?tab_name=recentlyupdated&start={start}&length={lenght}'.format(
                start=self.settings.start_count,
                lenght=self.settings.length_count
            )
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        items = UrlsCollectorItem()

        data = json.loads(response.text).get('data').get('list')
        for item in data:
            items['url'] = 'https://e27.co/startups/' + item.get('slug').strip()
            yield items

        if data:
            self.settings.start_count += len(data)

            next_page = 'https://e27.co/api/startups/?tab_name=recentlyupdated&start={}&length={}'
            yield response.follow(next_page.format(self.settings.start_count, self.settings.length_count), self.parse)
