import scrapy
import json
import os
import csv
import re
import maya

from ..items import DataCollectorItem
from random import choice

URLS_COUNT = 250


class DataCollectorSpider(scrapy.Spider):
    name = "data_collector"

    def __init__(self, *args, **kwargs):
        super(DataCollectorSpider, self).__init__(*args, **kwargs)

        with open(os.path.join(os.getcwd(), 'urls.csv')) as f:
            rows = csv.reader(f, delimiter=',')

            # skip header
            next(rows, None)

            urls = [url.pop() for url in rows if url]
            self.start_urls = []

            # checking for the necessary number of urls to prevent an infinite loop
            if len(urls) >= URLS_COUNT:

                # random choice for a URLS_COUNT urls and checking for a duplicates
                while len(self.start_urls) < URLS_COUNT:
                    url_pattern = 'https://e27.co/api/startups/get/?slug={}&data_type=general'
                    url = url_pattern.format(choice(urls).split('/')[-1])
                    self.start_urls.append(url) if url not in self.start_urls else None

    def parse(self, response):
        items = DataCollectorItem()
        regexp = r'\+?\d{0,3}[\s|\-]?\(?\d{1,3}\)?[\s|\-|\d]{8,10}'

        data = json.loads(response.text).get('data')

        tags = data.get('metas').get('market', '')
        short = data.get('metas').get('short_description', '')
        description = data.get('metas').get('description', '')
        urls = [data.get('metas').get(url)
                for url in ['linkedin', 'twitter', 'facebook']
                if data.get('metas').get(url)]

        try:
            found_month = data.get('metas').get('found_month', '')
            found_year = data.get('metas').get('found_year', '')

            # Checking for <= 0 - incorrect date element
            found_month = found_month if int(found_month) > 0 else ''
            found_year = found_year if int(found_year) > 0 else ''

            founded = str(found_month) + ', ' + str(found_year) \
                if found_month and found_year else found_month or found_year
        except (ValueError, Exception):
            founded = None

        items['company_name'] = data.get('name', ''),
        items['profile_url'] = 'https://e27.co/startups/' + data.get('slug', ''),
        items['company_website_url'] = data.get('metas').get('website', ''),
        items['location'] = ', '.join([location.get('text', '') for location in data.get('location')]),
        items['tags'] = ', '.join(tags) if isinstance(tags, list) else tags,
        items['founding_date'] = maya.parse(founded).datetime().strftime('%Y-%m-%d') if founded else '',
        items['urls'] = ', '.join(urls) if urls else '',
        items['emails'] = data.get('metas').get('email', ''),
        items['description_short'] = short,
        items['description'] = description

        # Removing '(', ')' and '+' for correct insert to sheet cell
        items['phones'] = (', '.join([phone.strip().replace('(', '').replace(')', '').replace('+', '')
                                      for phone in re.findall(re.compile(regexp), short)]) + ', ' +
                           ', '.join([phone.strip().replace('(', '').replace(')', '').replace('+', '')
                                      for phone in re.findall(re.compile(regexp), description)])).strip(', ')

        next_page = 'https://e27.co/api/startups/get/?id={}&data_type=teammembers'
        yield response.follow(next_page.format(data.get('id')), self.parse_team_members, meta={'items': items})

    @staticmethod
    def parse_team_members(response):
        items = response.meta.get('items')
        data = json.loads(response.text).get('data')

        founders = [item.get('name') for item in data if item.get('is_admin')]
        employee = [item.get('name') for item in data if not item.get('is_admin')]

        items['founders'] = ', '.join(founders) if founders else '',
        items['employee_range'] = ', '.join(employee) if employee else '',

        yield items
